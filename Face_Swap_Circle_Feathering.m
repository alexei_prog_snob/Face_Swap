%%
clear all,close all;
%% Face Detectector

I = imread('test3.jpg');
tic
Meas_Face=1;
Stop_Search_Face=1;
while Stop_Search_Face~=0
    FacesDetector_1 = vision.CascadeObjectDetector('FrontalFaceCART',...
                                              'MergeThreshold',Meas_Face);
    bboxes = step(FacesDetector_1, I);
    [m_mouth_1 n_eye_1]=size(bboxes);
    if m_mouth_1==2
        Stop_Search_Face=0;
    elseif m_mouth_1==0
        Stop_Search_Face=0;
    else
        Meas_Face=Meas_Face+1;
    end
end

%% Crop Two Faces

% Crop First Face
I_Face_1 = imcrop(I,bboxes(1,:));
I_Face_2 = imcrop(I,bboxes(2,:));


%% Detect Eyes And Mouth in wich face
Meas=1;
Stop_Search_eyes=1;
flag_eye_1=1;
flag_eye_2=1;
flag_mouth_1=1;
flag_mouth_2=1;

while Stop_Search_eyes~=0
    
    if flag_eye_1==1% Pair Eyes of fist face
          EyesDetector_1 = vision.CascadeObjectDetector('EyePairBig',...
                                                   'MergeThreshold',Meas);
                                               
          bboxes_eye_temp_1 = step(EyesDetector_1, I_Face_1);
          [m_eye_1 n_eye_1]=size(bboxes_eye_temp_1);
           if m_eye_1==1
                bboxes_eye_1=bboxes_eye_temp_1;
                flag_eye_1=0;
           end
    end      
                                              
    
    if flag_eye_2==1
          EyesDetector_2 = vision.CascadeObjectDetector('EyePairBig',...
                                               'MergeThreshold',Meas);
    
          bboxes_eye_temp_2 = step(EyesDetector_2, I_Face_2);
          [m_eye_2 n_eye_2]=size(bboxes_eye_temp_2);
          if m_eye_2==1
            bboxes_eye_2=bboxes_eye_temp_2;
            flag_eye_2=0;
          end
    end
    
    if flag_mouth_1==1
        MouthDetect_1 = vision.CascadeObjectDetector('Mouth',...
                                                     'MergeThreshold',Meas);
        bboxes_mouth_temp_1 = step(MouthDetect_1, I_Face_1);
        [m_mouth_1 n_mouth_1]=size(bboxes_mouth_temp_1);
        if m_mouth_1==1
            bboxes_mouth_1=bboxes_mouth_temp_1;
            flag_mouth_1=0;
        end
    end
    
    if flag_mouth_2==1
        MouthDetect_2 = vision.CascadeObjectDetector('Mouth',...
                                                     'MergeThreshold',Meas);                                         
        bboxes_mouth_temp_2 = step(MouthDetect_2, I_Face_2);                                                                       
        [m_mouth_2 n_mouth_2]=size(bboxes_mouth_temp_2); 
        if m_mouth_2==1
            bboxes_mouth_2=bboxes_mouth_temp_2;
            flag_mouth_2=0;
        end
    end

    if flag_eye_2==0 & flag_eye_1==0 & flag_mouth_2==0 & flag_mouth_1==0
        Stop_Search_eyes=0;
%     elseif m_mouth_2 | m_mouth_1 | m_eye_1 | m_eye_2
%         break;
    else
        Meas=Meas+7;
    end
end


%% First Face

% Bonderis of upper face for swap

Up_Face_One_Place(1)=bboxes_eye_1(1);
Up_Face_One_Place(2)=round(bboxes_eye_1(2)/2);
Up_Face_One_Place(3)=bboxes_eye_1(3);
Up_Face_One_Place(4)=bboxes_mouth_1(2)-round(bboxes_eye_1(2)/2)+bboxes_mouth_1(4);

% Swap First Face I_Face_2 on I_Face_1
imgb_first = im2double(I_Face_1);% First Face
imga_second = im2double(I_Face_2);% Second Face
imga_second = imresize(imga_second,[size(imgb_first,1) size(imgb_first,2)]);

% Upper Face Swap
mask_Up_Face12 = zeros(size(imgb_first));

imshow(mask_Up_Face12)
hEllipse = imellipse(gca,Up_Face_One_Place); % Second argument defines
mask_Up_Face = double(hEllipse.createMask());

imshow(mask_Up_Face)
maskb_Up_Face = 1-mask_Up_Face;
blurh = fspecial('gauss',30,15); % feather the border
mask_Up_Face = imfilter(mask_Up_Face,blurh,'replicate');
maskb_Up_Face = imfilter(maskb_Up_Face,blurh,'replicate');

imgo1_first_face(:,:,1) = mask_Up_Face.*imga_second(:,:,1)+maskb_Up_Face.*imgb_first(:,:,1);
imgo1_first_face(:,:,2) = mask_Up_Face.*imga_second(:,:,2)+maskb_Up_Face.*imgb_first(:,:,2);
imgo1_first_face(:,:,3) = mask_Up_Face.*imga_second(:,:,3)+maskb_Up_Face.*imgb_first(:,:,3);

%% Second Face

% Bonderis of upper face for swap
Up_Face_Two_Place(1)=bboxes_eye_2(1);
Up_Face_Two_Place(2)=round(bboxes_eye_2(2)/2);
Up_Face_Two_Place(3)=bboxes_eye_2(3);
Up_Face_Two_Place(4)=bboxes_mouth_2(2)-round(bboxes_eye_2(2)/2)+bboxes_mouth_2(4);

%


% Swap First Face I_Face_1 on I_Face_2
imgb_second = im2double(I_Face_2);% Second Face
imga_first = im2double(I_Face_1);% First Face
imga_first = imresize(imga_first,[size(imgb_second,1) size(imgb_second,2)]);

% Upper Face Swap
mask_Up_Face12 = zeros(size(imga_first));
%mask_Up_Face(Up_Face_Two_Place(2):(Up_Face_Two_Place(2)+Up_Face_Two_Place(4)),...
%    Up_Face_Two_Place(1):(Up_Face_Two_Place(1)+Up_Face_Two_Place(3)),:) = 1;
imshow(mask_Up_Face12)
hEllipse = imellipse(gca,Up_Face_Two_Place); % Second argument defines
mask_Up_Face = double(hEllipse.createMask());

imshow(mask_Up_Face)
maskb_Up_Face = 1-mask_Up_Face;
blurh = fspecial('gauss',30,15); % feather the border
mask_Up_Face = imfilter(mask_Up_Face,blurh,'replicate');
maskb_Up_Face = imfilter(maskb_Up_Face,blurh,'replicate');

imgo2_second_face(:,:,1) = mask_Up_Face.*imga_first(:,:,1)+maskb_Up_Face.*imgb_second(:,:,1);
imgo2_second_face(:,:,2) = mask_Up_Face.*imga_first(:,:,2)+maskb_Up_Face.*imgb_second(:,:,2);
imgo2_second_face(:,:,3) = mask_Up_Face.*imga_first(:,:,3)+maskb_Up_Face.*imgb_second(:,:,3);
% % Mouth Face Swap
% imgb_mouth = imgo2_second_face;% First Face
% 
% mask_Mouth=zeros(size(imga_first));
% mask_Mouth(bboxes_mouth_2(2):(bboxes_mouth_2(2)+bboxes_mouth_2(4)),...
%     bboxes_mouth_2(1):(bboxes_mouth_2(1)+bboxes_mouth_2(3)),:) = 1;
% 
% maskb_Mouth = 1-mask_Mouth;
% 
% blurh = fspecial('gauss',30,15); % feather the border
% mask_Mouth = imfilter(mask_Mouth,blurh,'replicate');
% maskb_Mouth = imfilter(maskb_Mouth,blurh,'replicate');

% imgo2_second_face(:,:,1) = mask_Mouth.*imga_first(:,:,1)+maskb_Mouth.*imgb_mouth(:,:,1);
% imgo2_second_face(:,:,2) = mask_Mouth.*imga_first(:,:,2)+maskb_Mouth.*imgb_mouth(:,:,2);
% imgo2_second_face(:,:,3) = mask_Mouth.*imga_first(:,:,3)+maskb_Mouth.*imgb_mouth(:,:,3);
%% Palcing the swap faces on original image
New_I=im2double(I);

New_I(bboxes(1,2):(bboxes(1,2)+bboxes(1,4)),...
    bboxes(1,1):(bboxes(1,1)+bboxes(1,3)),:)=imgo1_first_face;

New_I(bboxes(2,2):(bboxes(2,2)+bboxes(2,4)),...
    bboxes(2,1):(bboxes(2,1)+bboxes(2,3)),:)=imgo2_second_face;
toc
%% Plot THe faces, Eyes and mouths that was detected

IFaces_1 = insertObjectAnnotation(I_Face_1, 'rectangle', bboxes_eye_1, 'Eyes');
IFaces_2 = insertObjectAnnotation(I_Face_2, 'rectangle', bboxes_eye_2, 'Eyes');
IFaces_3 = insertObjectAnnotation(IFaces_1, 'rectangle', bboxes_mouth_1, 'Mouth');
IFaces_4 = insertObjectAnnotation(IFaces_2, 'rectangle', bboxes_mouth_2, 'Mouth');

New_I1=I;

New_I1(bboxes(1,2):(bboxes(1,2)+bboxes(1,4)),...
    bboxes(1,1):(bboxes(1,1)+bboxes(1,3)),:)=IFaces_3;

New_I1(bboxes(2,2):(bboxes(2,2)+bboxes(2,4)),...
    bboxes(2,1):(bboxes(2,1)+bboxes(2,3)),:)=IFaces_4;

IFaces = insertObjectAnnotation(New_I1, 'rectangle', bboxes, 'Face');
%%
imshow(IFaces)
figure
imshow(New_I)
%% Final Plots
% subplot(121),imshow(IFaces),title('Original image with detections');
% subplot(122),imshow(New_I),title('Swapped Faces');
